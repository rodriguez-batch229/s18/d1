//  [Section] - Paramenters and Arguments
// Functions in JS are lines/blocks of code that tells our device application to perform a certean task
// Last session we learned how to use the basic function

function printInput() {
    let nickname = prompt("Enter your nickname");
    console.log("Hi!, ", nickname);
}

// printInput();

// In some cases a basic function may not be ideal
// For other cases, functions can also process data directly into it instead

// Consider this function
// Parameter is located inside the "(paramenter)" after the funtion name
function printName(name) {
    console.log("My name is " + name);
}

// Argument is located in the invocation of th function. The data then will be passed on the parameter which can be used inside the code blocks of a particular function
printName("Juana");
printName("John");
printName("Jane");

// variables can also be passed as an argument
let sampleVariable = "Yui";

printName(sampleVariable);

// Function arguments cannot be used by a function if there are no parameters provided

function checkDivisibiltyBy8(num) {
    let remainder = num % 8;
    console.log("The remainder of " + num + " divided by 8 is: " + remainder);
    let isDivisibleBy8 = remainder === 0;
    console.log("Is " + num + " divisible by 8?");
    console.log(isDivisibleBy8);
}
checkDivisibiltyBy8(64);
checkDivisibiltyBy8(28);

// function as argument

function argumentFunction() {
    console.log(
        "This function was passed as an argument before the message was printed"
    );
}

function invokeFunction(argumentFunction) {
    argumentFunction();
}
invokeFunction(argumentFunction);
// finding more info about a function, we can use console.log(function_name);

console.log(argumentFunction);

// Function - Multiple Parameters
// Multiple "arguments" will correspond to the number of "parameters"

function createFullName(firstName, middleName, lastName) {
    console.log(`${firstName} ${middleName} ${lastName}`);
}

createFullName("Juan", "Dela", "Cruz");
// "Juan" will be stored in the parameter "firstName"
// "Dela" will be stored in the parameter "middleName"
//  "Cruz" will be stored in the parameter "lastName"

// the last name parameter will return undefine value
createFullName("Juan", "Dela");
// "Hello" will be disregarded since the function is expectiong 3 arguments only.
createFullName("Juan", "Dela", "Cruz", "Hello");

let firstName = "John",
    middleName = "Doe",
    lastName = "Smith";

createFullName(firstName, middleName, lastName);

// Parameter names are just names to refere to the argument.
// The order of the argument is the same order  to the order of the parameters

function printFullName(middleName, firstName, lastName) {
    console.log(`${firstName} ${middleName} ${lastName}`);
}
printFullName("Juan", "Dela", "Cruz");

// The return statement

function returnFullName(firstName, middleName, lastName) {
    console.log("Test console message");
    return firstName + " " + middleName + " " + lastName;
    console.log("This mesage will not be printed");
}

let completeName = returnFullName("Jeffrey", "Smith", "Bezos");

console.log(completeName);
// returnFullName("Jeffrey", "Smith", "Bezos");
// This will not work if our function is expectiong a return value, returned values shoud be restored in a variable.

console.log(returnFullName(firstName, middleName, lastName));

// You can also create a variable inside the function to contain the result and return that variable instead

function returnAddress(city, country) {
    let fullAddress = city + ", " + country;
    return fullAddress;
}

let myAddress = returnAddress("Cebu City", "Philippines");
console.log(myAddress);

// On the other hand, when a function only has console.log() to display its results will return undefined instead

function printPlayerInfo(userName, level, job) {
    console.log("Username " + userName);
    console.log("Level: " + level);
    console.log("Job: " + job);
}

let user1 = printPlayerInfo("knight_white", 95, "Paladin");
console.log(user1);
// Returns undefined because printPlayerInfo returns nothing.

printPlayerInfo("knight_white", 95, "Paladin");
